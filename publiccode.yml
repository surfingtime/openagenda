# This repository adheres to the publiccode.yml standard by including this 
# metadata file that makes public software easily discoverable.
# More info at https://github.com/italia/publiccode.yml

publiccodeYmlVersion: '0.2'
name: OpenAgenda
releaseDate: '2019-06-14'
url: 'https://gitlab.com/opencontent/openagenda.git'
applicationSuite: OpenPA
softwareVersion: '1.21'
developmentStatus: stable
softwareType: standalone/web
platforms:
  - web
usedBy:
  - Comune di Ala (TN)
  - Comune di Rovereto (TN)
  - Comune di Udine (UD)
  - Comune di Bolzano (BZ)
  - Comune di Casarsa della Delizia (PN)
  - Comunità Valsugana e Tesino (TN)
  - Comune di Palermo (PA)
  - Comune di Ville D'Anaunia (TN)
  - Comune di Brentonico (TN)
  - Comune di Vallelaghi (TN)
  - Comune di Mori (TN)
landingURL: 'https://gitlab.com/opencontent/openagenda'
logo: screenshots/OpenAgenda.png
categories:
  - collaboration
  - communications
  - event-management
maintenance:
  type: internal
  contacts:
    - name: Gabriele Francescotto
      email: gabriele.francescotto@opencontent.it
      phone: +39 0461 917437
legal:
  license: GPL-2.0-only
  mainCopyrightOwner: Comune di Ala
  repoOwner: Opencontent SCARL
intendedAudience:
  scope:
    - culture
    - tourism
    - local-authorities
  countries:
    - it
localisation:
  localisationReady: true
  availableLanguages:
    - it
    - de
it:
  riuso:
    codiceIPA: c_a116
  conforme:
    lineeGuidaDesign: true
    modelloInteroperabilita: true
    gdpr: true
    misureMinimeSicurezza: true
description:
  it:
    shortDescription: Calendario degli eventi partecipato dai cittadini
    longDescription: >
      E’ un calendario eventi partecipato dai soggetti che animano la vita
      culturale e sociale di una comunità locale; associazioni, biblioteche e
      musei pubblicano i propri eventi in un unico calendario che ne facilita la
      diffusione multicanale, in coordinamento con l'ente locale; un modo per
      rafforzare le relazioni tra amministrazione e territorio, secondo il
      paradigma dell'_OpenGovernment_.


      Consente di prevenire le sovrapposizioni di date e di raggruppare le
      iniziative per tematiche in base agli interessi di cittadini e turisti,
      che vi accedono tramite pc, tablet, smartphone, o scaricando una locandina
      sintetica corredata di un QRcode per evento, generata automaticamente. I
      profili delle associazioni e gli stessi eventi vengono rilasciati come
      _Open Data_ e resi disponibili via API.
      
      E' stato inizialmente sviluppato da Opencontent per conto del Comune di
      Ala; il percorso di design è stato condotto in collaborazione con il
      Consorzio dei Comuni Trentini, che ha contribuito a definirne le
      specifiche finalizzate a favorire il riuso dell'applicazione da parte dei
      comuni trentini.
    documentation: 'https://manuale-openagenda.readthedocs.io/'
    apiDocumentation: 'https://documenter.getpostman.com/view/7046499/S17tPncK?version=latest'
    features:
      - Accesso per le associazioni che inseriscono i propri eventi
      - Moderazione degli eventi da parte dell'ente
      - Filtri in base a destinatari degli eventi, interessi degli utenti, luoghi di riferimento
      - Visualizzazione degli eventi come elenco, mappa, calendario
      - Calendari tematici, possibilità di aggregare eventi per destinatari o tipologia
      - Produzione di un programma eventi in pdf con link (qrcode) agli eventi sul sito
      - Conformità agli standard europei sulla strutturazione dei dati (ISA²) 
      - Conformità agli standard di accessibilità e alle linee guida AgID
      - Generazione automatica di Open Data
      - Dati strutturati secondo standard e regole di interoperabilità semantica
      - Promozione dei profili delle associazioni e degli eventi attraverso il
        paradigma Open Data
    genericName: Calendario eventi collaborativo
    screenshots:
      - screenshots/Solo-gli-eventi-di-interesse-per-il-cittadino-i-filtri_imagefullwide.png
      - screenshots/Come-si-presenta-il-calendario-al-cittadino_imagefullwide.png
      - screenshots/Navigazione-sulla-mappa-su-cui-applicare-i-filtri_imagefullwide.png
      - screenshots/Le-associazioni-del-territorio-sulla-mappa_imagefullwide.png
      - screenshots/Le-associazioni-sulla-mappa-grazie-alla-georeferenziazione_imagefullwide.png
      - screenshots/Locandina-automatica-con-eventi-dotati-di-QRcode_imagefullwide.png
      - screenshots/Il-registro-delle-associazioni-finalmente-in-ordine_imagefullwide.png
      - screenshots/Un-cruscotto-per-moderare-e-monitorare-gli-eventi_imagefullwide.png
      - screenshots/Moderazione-dei-commenti-dei-partecipanti_imagefullwide.png
      - screenshots/Il-calendario-di-pianificazione-degli-eventi-per-l-amministratore_imagefullwide.png
      - screenshots/Gestire-i-profili-delle-associazioni_imagefullwide.png
      - screenshots/Compilazione-guidata-dell-evento-anche-da-mobile_imagefullwide.png
      - screenshots/Gestione-del-workflow-di-approvazione-dei-singoli-eventi-per-l-amministratore_imagefullwide.png
      - screenshots/Impaginare-la-bruchure-via-web_squarethumb.png
      - screenshots/Per-i-programmatori-creare-una-APP-affidabile-con-dati-di-alta-qualita_squarethumb.png
