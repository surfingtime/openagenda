v2.0

| Production Changes                    | From       | To       | Compare                                                                                     |
|---------------------------------------|------------|----------|---------------------------------------------------------------------------------------------|
| arnebratt/batchtool                   | 1.1        | 1.2.1    | [...](https://github.com/Opencontent/Batchtool/compare/1.1...1.2.1)                         |
| aws/aws-sdk-php                       | 3.91.1     | 3.91.4   | [...](https://github.com/aws/aws-sdk-php/compare/3.91.1...3.91.4)                           |
| brookinsconsulting/gwutils            | 1          | 3d25c19  | [...](https://github.com/Opencontent/gwutils/compare/1...3d25c19)                           |
| ezsystems/ezfind-ls                   | v2015.01.0 | 2592f16  | [...](https://github.com/Opencontent/ezfind/compare/v2015.01.0...2592f16)                   |
| ezsystems/ezgmaplocation-ls-extension | f290c66    | 5.3.1.1  | [...](https://github.com/Opencontent/ezgmaplocation-ls-extension/compare/f290c66...5.3.1.1) |
| ezsystems/ezmbpaex-ls                 | v5.3.3     | 5.3.3.1  | [...](https://github.com/Opencontent/ezmbpaex/compare/v5.3.3...5.3.3.1)                     |
| ezsystems/ezodf-ls                    | v5.3.8     | 5.3.8.1  | [...](https://github.com/Opencontent/ezodf/compare/v5.3.8...5.3.8.1)                        |
| ezsystems/ezpublish-legacy            | e69dab2    | 372929d  | [...](https://github.com/Opencontent/ezpublish-legacy/compare/e69dab2...372929d)            |
| ezsystems/ezsurvey-ls                 | v5.3.3     | ed79c54  | [...](https://github.com/Opencontent/ezsurvey/compare/v5.3.3...ed79c54)                     |
| gggeek/ggwebservices                  | 0.14.2     | 01a5f39  | [...](https://github.com/Opencontent/ggwebservices/compare/0.14.2...01a5f39)                |
| mugoweb/mugoobjectrelations           |            | d80a0ea  | [...](https://github.com/Opencontent/mugoobjectrelations/compare/...d80a0ea)                |
| netgen/ngopengraph                    | 1.3.1      | 119618c  | [...](https://github.com/Opencontent/ngopengraph/compare/1.3.1...119618c)                   |
| netgen/ngpush                         | 1.3        | 99c87d4  | [...](https://github.com/Opencontent/ngpush/compare/1.3...99c87d4)                          |
| opencontent/bfsurveyfile              | 1.0        | 2.1      | [...](https://github.com/Opencontent/bfsurveyfile/compare/1.0...2.1)                        |
| opencontent/cjwnewsletter-ls          | 2.3.3      | 3.0.1    | [...](https://github.com/Opencontent/cjw_newsletter/compare/2.3.3...3.0.1)                  |
| opencontent/easyvocs_connector-ls     | 65f5e25    | 97e5043  | [...](https://github.com/OpencontentCoop/easyvocs_connector/compare/65f5e25...97e5043)      |
| opencontent/enhancedezbinaryfile-ls   | 1.0        | 2.0      | [...](https://github.com/Opencontent/enhancedezbinaryfile/compare/1.0...2.0)                |
| opencontent/ezflip-ls                 | v2.x-dev   | 3.1      | [...](https://github.com/OpencontentCoop/ezflip/compare/v2.x-dev...3.1)                     |
| opencontent/ezflowplayer-ls           | 1.0.1      | 2.0      | [...](https://github.com/Opencontent/ezflowplayer/compare/1.0.1...2.0)                      |
| opencontent/occhangeobjectdate-ls     | 1.1        | 2.0      | [...](https://github.com/OpencontentCoop/occhangeobjectdate/compare/1.1...2.0)              |
| opencontent/occsvimport-ls            | 1.2.2      | 2.1      | [...](https://github.com/OpencontentCoop/occsvimport/compare/1.2.2...2.1)                   |
| opencontent/oceditorialstuff-ls       | 1.2.2      | 2.1.2    | [...](https://github.com/OpencontentCoop/oceditorialstuff/compare/1.2.2...2.1.2)            |
| opencontent/ocgdprtools-ls            | b50d386    | 1.2.1    | [...](https://github.com/OpencontentCoop/ocgdprtools/compare/b50d386...1.2.1)               |
| opencontent/ocmultibinary-ls          | 1.2.1      | 2.0.1    | [...](https://github.com/OpencontentCoop/ocmultibinary/compare/1.2.1...2.0.1)               |
| opencontent/ocoperatorscollection-ls  | 1.3        | 2.1      | [...](https://github.com/OpencontentCoop/ocoperatorscollection/compare/1.3...2.1)           |
| opencontent/ocsensor-ls               | 2.8.3      | 3.0      |                                                                                             |
| opencontent/ocsupport-ls              | 3d1b588    | 49a42aa  | [...](https://github.com/OpencontentCoop/ocsupport/compare/3d1b588...49a42aa)               |
| opencontent/openpa-ls                 | 2.30.1     | v3.x-dev | [...](https://github.com/OpencontentCoop/openpa/compare/2.30.1...v3.x-dev)                  |
| opencontent/openpa_booking-ls         | 3.7        | v4.x-dev | [...](https://github.com/OpencontentCoop/openpa_booking/compare/3.7...v4.x-dev)             |
| opencontent/openpa_designitalia-ls    | 1.23.6.1   | 1.23.8   | [...](https://github.com/OpencontentCoop/openpa_designitalia/compare/1.23.6.1...1.23.8)     |
| opencontent/openpa_importers-ls       | 1.7.5      | v2.x-dev |                                                                                             |
| opencontent/openpa_newsletter-ls      | 2.6.3      | 2.6.4    | [...](https://github.com/OpencontentCoop/openpa_newsletter/compare/2.6.3...2.6.4)           |
| opencontent/openpa_sensor-ls          | 1.5        | 1.5.1    | [...](https://github.com/OpencontentCoop/openpa_sensor/compare/1.5...1.5.1)                 |
| opencontent/openpa_theme_2014-ls      | 2.11.9     | 2.11.10  | [...](https://github.com/OpencontentCoop/openpa_theme_2014/compare/2.11.9...2.11.10)        |
| paradoxe/paradoxpdf                   | 1          | 7ab4eb7  | [...](https://github.com/Opencontent/paradoxpdf/compare/1...7ab4eb7)                        |
| ezsystems/ezwebin-ls-extension        | NEW        | eba2a53  |                                                                                             |
| symfony/polyfill-php73                | NEW        | d1fb4ab  |                                                                                             |

### [arnebratt/batchtool changes between 1.1 and 1.2.1](https://github.com/Opencontent/Batchtool/compare/1.1...1.2.1)
 * added change_word function 
 * added excluded words 
 * fixed problem with language version 
 * Moving main code to a class, and basing filters/operations on classes instead of files. 
 * Adding exceptions for error situations 
 * Fixing and vs.or logic etc. 
 * Adding documentation and fixing some error situations 
 * Merge branch 'autoload-support'  Conflicts: 	ezinfo.php 
 * Including create new translation operation 
 * Added support for choosing "and" and "or" logic between local filters 
 * Merge pull request #9 from arnebratt/translations-logic  Added support for choosing "and" and "or" logic between local filters 
 * Added attribute filter to node fetch filter 
 * Adding hide operation 
 * Enable hide operation 
 * Fixed bug 
 * Merge pull request #8 from piotr-szczygiel/master  added change_word function 
 * Adjusting add location operation 
 * Add option to specify more than one parent folder, and fixed add location operation 
 * Added sub field support for the nodelistname operation 
 * A couple minor fixes to the move operation 
 * Adding a node merge operation to merge two different nodes of an object 
 * Added selection of node ids, as well as subtree copy operation 
 * Fixed buggy attribute filter on fetchnodelist 
 * Fixed nodecopy bug, and added use_main_node parameter to fetches 
 * Set nodelistname to show object name only if optional fields are not specified 
 * Fixed language on node change attribute operation 
 * Add possibility to use : and ; in attribute change text 
 * fix PHP 4 constructors 
 * Create composer.json 

### [aws/aws-sdk-php changes between 3.91.1 and 3.91.4](https://github.com/aws/aws-sdk-php/compare/3.91.1...3.91.4)
 * Update models for release 
 * 3.91.2 release 
 * Update models for release 
 * 3.91.3 release 
 * Pass Expect http config in GuzzleV5 handler (#1733) 
 * add changelog 
 * Merge pull request #1760 from srchase/proxy-remove-expect  pass through expect on http config 
 * Update models for release 
 * 3.91.4 release 

### [ezsystems/ezfind-ls changes between v2015.01.0 and 2592f16](https://github.com/Opencontent/ezfind/compare/v2015.01.0...2592f16)
 * Not sure what the purpose of the name inside the switch statement. I can't find any documentation on it so I am assuming it is an error. Feel free to enlighten me if I am wrong. 
 * Fix: EZP-23878 No result when I found 1 location in several languages with multicore ezfind 
 * Fix EZP-24021: Link is wrong with non-main nodes using 'subtree_array' 
 * Merge pull request #190 from ezsystems/ezp-24021-subtree_array_main_nodes  Fix EZP-24021: Link is wrong with non-main nodes using 'subtree_array' 
 * Fix EZP-23644: add proper handling for hidden nodes in subtree fetches and multiple locations 
 * Merge pull request #181 from ezsystems/EZP-23644  Fix EZP-23644: add proper handling for hidden nodes in subtree fetches a... 
 * Fix EZP-21831: eZFind: customfields with asObject=false 
 * Updated a misleading comment. 
 * Merge pull request #191 from ezsystems/ezp-21831-custom_fields_asobject_false  Fix EZP-21831: eZFind: customfields with asObject=false 
 * Merge pull request #187 from blankse/patch-1  Fix: EZP-23878 No result when I found 1 location in several languages wi... 
 * EZP-21764: Don't instantiate eZINI outside of classes 
 * Merge pull request #129 from jeromegamez/EZP-21764_unintentional_ezini_instances  EZP-21764: Don't instantiate eZINI outside of classes 
 * Fix EZP-24553: no results for exact phrases with camelCase words 
 * Merge pull request #196 from joaoinacio/EZP-24553_camelcase_exact_matches  Fix EZP-24553: no results for exact phrases with camelCase words 
 * Fix EZP-24739: ezFind IndexBoost not working in some locales  Closes #197 
 * Fix: EZP-26324 Wrong translation in dut-NL 
 * Merge pull request #199 from blankse/patch-2  Fix: EZP-26324 Wrong translation in dut-NL 
 * Fix EZP-27738: Return the field name as field value in facet_fields list 
 * Merge pull request #210 from crevillo/COM-20019  EZP-27738: Return the field name as field value in facet_fields list 
 * Calling parent constructor correctly 
 * EZP-26804: Search with section id list filter 
 * Add ezpublish-legacy requirement to >= 2017.10 
 * Merge pull request #212 from pkamps/php7_constructor_fix  Calling parent constructor correctly for 2017.10 / PHP7 compatibility 
 * Merge pull request #201 from datafactory/EZP-26804  EZP-26804: Search with section id list filter 
 * Merge pull request #43 from harmstyler/patch-1  Not sure what the purpose of the name inside the switch statement. I can... 
 * fix php 7 constructors 
 * fix php 7 constructors 

### [ezsystems/ezgmaplocation-ls-extension changes between f290c66 and 5.3.1.1](https://github.com/Opencontent/ezgmaplocation-ls-extension/compare/f290c66...5.3.1.1)
 * GoogleMaps API Key added for edit and view templates 
 * quotes removed from ezgmaplocation.ini.append.php 
 * Cast integers in fetch 
 * Fix string limit in address field 
 * Requirements bump for 5.4.0-beta1 
 * fix PHP 4 constructors 
 * Remove suggest in composer 
 * Rebased on ez 

### [ezsystems/ezpublish-legacy changes between e69dab2 and 372929d](https://github.com/Opencontent/ezpublish-legacy/compare/e69dab2...372929d)
 * Merge pull request #1265 from lolautruche/fix/cloneContentAttributes  Fix EZP-26449: Wrong version translation list in eZContentOperationCollection 
 * Fix EZP-26405: SQL Injection in Search Component (ezsearchengine) (#105)  (cherry picked from commit 4005a9092178cc485868b1067a2101c59a80be4f) 
 * Fix EZP-26798: Query parameters get lost when being redirected with mobile device detect filter 
 * Making sure all affected PHP versions are handled 
 * Fix EZP-26832: Enabling editor causes error on links with "&" (#1278) 
 * Setting to have different cache based on the request protocol 
 * Fix EZP-26405: SQL Injection in Search Component (ezsearchengine)  Follow-up to 6d926593fd5c00028b8d379c7273898b3055beed which was incomplete.  (cherry picked from commit 3e549c815d7c35347567824e1a39890ed4bc919f) (cherry picked from commit b080ebb58f80b9cffb803464f36db261d8c4d9ec) (cherry picked from commit 4a7824949ecbb0fa70b15b22811f9f84af19ea0a) 
 * Fix EZP-26659: The package component is vulnerable to arbitrary file upload  (cherry picked from commit 789e5d4d2bb6ec81a3ee0e8b5b832861d0d14d47) (cherry picked from commit 03145783c1e35f4bf83e9ef08d53b0dbaabf4ce3) (cherry picked from commit d68b26ef230434618c481c53082e9344c7ccc417) 
 * EZP-27000: Remove uploader.swf from all repos/branches  (cherry picked from commit fe1750394e05558ab305f25040a018d6e2872acd) 
 * Fix EZP-26911: Embedded PDF files are downloadable though they are in trash  (cherry picked from commit b77fc1552e2749389a97a0d9a9198d7949640de4) (cherry picked from commit aca9b5a3361192fe790b501de71ec114ae779b32) (cherry picked from commit 4b11ff45152a32734868f85cada4f29463137c84) 
 * Merge pull request #1272 from blankse/patch-1  Fix EZP-26798: Query parameters get lost when being redirected with m… 
 * Merge pull request #1275 from pkamps/fix-var-export-normalize  Making sure all affected PHP versions are handled 
 * Merge pull request #1284 from pkamps/request_protocol_as_cache_key  Setting to have different cache based on the request protocol 
 * EZP-27092: Remove eZAutoLink operator usage of preg_replace and PREG_REPLACE_EVAL (#1286)  * Fix EZP-27092: Remove eZAutoLink operator usage of preg_replace and PREG_REPLACE_EVAL  * Fix EZP-27092: Remove eZAutoLink operator usage of preg_replace and PREG_REPLACE_EVAL add unit tests 
 * Add state change/assign to the audit log (#1285) 
 * EZP-15983 - Only include ezp_override when present (#1270)  When generating autoloads on a fresh ezpublish-legacy install through Symfony, the missing file raises a notice which is caught by the Symfony exception handler - This means that the autoloads can't be generated through Symfony, unless they have previously been generated by legacy.  This change is trivial and mitigates the issue.  Duplicate/response to https://jira.ez.no/browse/EZP-15983 - Please don't hate me for using a file_exists @bdunogier! 
 * EZP-27011: Solr index not updated when changing a content priority (#1283)  Edited & Tested by @vnivuahc, ref: https://github.com/netgen/ezplatformsearch/issues/11 
 * Fix EZP-27321: Update TinyMCE to 3.5.12 and patch for "Uncaught DOMEx… (#1297)  * Fix EZP-27321: Update TinyMCE to 3.5.12 and patch for "Uncaught DOMException: Failed to execute 'setBaseAndExtent' on 'Selection': There is no child at offset 1."  * Fix EZP-27321: Synchronization of eztable plugin with table plugin after update TinyMCE to 3.5.12  * Fix EZP-27321: Synchronization of ez theme plugin with advanced theme after update TinyMCE to 3.5.12 
 * EZP-27360: Make Template Operators utf8 friendly (#1298) 
 * Fix wrong message text and grammar in ezexec.php 
 * FIX EZP-27306: Display modifier of published version in edit view and in version view (#1296) 
 * Fix EZP-27254: "Unknown relation type 0." error when access content object from REST API response (#1295) 
 * Added community badges (#1304) 
 * EZP-27427 Cache generation performance improvement (#1303)  * EZP-27427 improving cache generation performance by limiting redundant db queries  * EZP-27427 styling changes  * EZP-27427 adding comment  * EZP-27427 improving comment's content 
 * EZP-26013 moving commit before method to fix relations (#1306) 
 * Fix EZP-27474: Unable to add the same URL alias for other languages (#1302) 
 * EZP-25916: fixing deadlock issues on clustering cache (#1301)  * EZP-25916: handling deadlock / lock wait timeout errors coming from mysql  * EZP-25916 adding more descriptive comment  * EZP-25916 typo fix  * EZP-25916 changing result format to meet expected one  * EZP-25916 changing comment 
 * Fix EZP-24919: Clear view/content cache when an object gets re-indexed (#1210) 
 * EZP-27091: Added a settings to mark data collections as a "containing sensitive data" (#1308)  * EZP-27091: Added a "CollectSensitiveData" and "CollectSensitiveDataList" settings to mark data collections as a containing sensitive data  * fixup! EZP-27091: Added a "CollectSensitiveData" and "CollectSensitiveDataList" settings to mark data collections as a containing sensitive data 
 * Fix escaping for custom tag attribute values (#1289) 
 * EZP-27714: Replaced wrong variable name in eZTemplateMultiPassParser class  Using $tagPosition brings a notice that it does not exists => the variable to use is $tagPos 
 * Fix EZP-24800: Support "allowed classes" limitation for object relation fields (#1201)  * Fix EZP-24800: Support 'allowed classes' limitation for object relation (singular) fields  * Fix object relation browse mode class constraint list when no classes are selected 
 * Unit test db setup (#1276) 
 * Handle non-numeric image size keys better. (#1290)  Currently the fallback when looking for icon sizes grabs the icon with the key `0`, but there are instances where there is nothing set at that key. I assume the author was meaning to grab the first icon size in the array, so I propose switching this to use reset. 
 * EZP-26070: Add support for content/publish policy (#1307) 
 * EZP-27735 Solr index not updated immediately after change content priority 
 * EZP-27737: Load correct groups/roles in ezuser (#1311)  * Load correct groups/roles in ezuser  The fixes a regression after https://github.com/ezsystems/ezpublish-legacy/commit/134b652e66a399743ba158bf06c101c8abca7bd4  * Retain original behavior on return 
 * EZP-26013 moving commit before method which publishes content (#1310)  * EZP-26013 moving commit before method which publishes content  * EZP-26013 clearing the cache again after db transaction is commited  * EZP-26013 adding new lines to keep amount of changes smaller 
 * EZP-27746: Obsolete object relations not cleaned up (#1314)  * EZP-25386: cleanup/sync ezobjrelation/list type CS  * Fix EZP-25396: Update object relations (list) on publish  * Fix EZP-25848: PostgreSQL regression in objectrelation/list  * Fix EZP-27493: Typos in kernel/classes/datatypes/{ezobjectrelation/ezobjectrelationtype.php,ezobjectrelationlist/ezobjectrelationlisttype.php} 
 * [Composer] Specify conflict with kernel older then 6.11  As part of [2017.08](https://github.com/ezsystems/ezpublish-legacy/milestone/1) milestone we are currently merging a few new features that will cause issues if they are used together with older kernel installs like 6.7 and 5.4.  This was suggested by @emodric [on ongoing](https://github.com/ezsystems/LegacyBridge/pull/103) work to simplify legacy bridge install, where simplifying also tries to avoid people ending up in non working package combinations. 
 * [Composer] Add branch alias in order to be able to resolve dependencies on legacy bridge pre release 
 * Merge pull request #1313 from ezsystems/2017.08_kernel_conflict  [Composer] Specify conflict with kernel older then 6.11 
 * [Composer] Update demo to 5.4 and add recommendation for ext-openssl 
 * [version] Update version info for master 
 * Fix EZP-25519: cleanupversions does not scale 
 * Fix EZP-26170: Input parsing causes DOMException: Invalid Character Error (#102) 
 * Fix EZP-25242: regression test 
 * Fix EZP-25242: Password change resets to default 
 * Fix EZP-25243: HTML entity umlaut &#xe4; breaks literal text block 
 * Fix EZP-25469: 'ezurl_object_link' table not updated after link deletion (#89) 
 * EZP-26522: index moved treenode after transaction commited 
 * Fix EZP-25796: URI map matcher and utf8 generates broken urls 
 * EZP-27313: Ensure reverse URL translation is not affected by PathPrefix (#113) 
 * Fix EZP-25481: XSS in backoffice content/search 
 * Fix EZP-25199: Files uploaded using multiupload before fix from EZP-23739 will continue without extension 
 * Fix EZP-25199: Files uploaded using multiupload before fix from EZP-23739 will continue without extension  Part 2: Make sure the script works even if you have run the old version of it before. 
 * Fix EZP-26805: non-break update script missing version. 
 * fixup! Fix EZP-25796: URI map matcher and utf8 generates broken urls 
 * Merge pull request #1312 from kmadejski/EZP-27735_Solr_index_not_updated_immediately_after_change_content_priority  EZP-27735 Solr index not updated immediately after change content priority 
 * [Composer] Relax kernel conflict for dev install 
 * Avoid conflicts with dev-master of kernel   Because of bug in composer (https://github.com/composer/composer/issues/5505), where alias of dev-master (and possible other dev branches) is not resolved on `conflicts` statements, we need to keep the comparison here under dev-master version number (9999999-dev).  Upper bound could have been set to 9999, but in case of future community releases of kernel, opted for 2017.08 which means 6.11 here. 
 * Merge pull request #1320 from ezsystems/relax_conflict  [Composer] Avoid conflict with dev-master of kernel 
 * EZP-26890: hiding content tree menu on error (#117)  * EZP-26890: hiding content tree menu on error (cherry picked from commit d1f8d2336e1c430c5679337efaac42db3a5a81cf) 
 * EZP-27804: Constructor refactoring (PHP4 to PHP5 __construct()) (#1233)  * Removed empty constructors  * Added __construct() constructor for popular classes  … and marked the old style constructors as deprecated. This is done to not break third party extensions that don't call the parent constructor with parent::__construct()  * Removed constructors that just call the parent constructor  * Replaced PHP4 constructors and calls with PHP5 constructors  * Fix broken UTF-8 chars  * Rename forgotten PHP4 constructor  * Restory empty constructors 
 * [WIP][Travis] Setup PHP7 testing on travis, bump requriment to PHP 5.5  Notes: - Minimum version for use with eZ Platform is PHP 5.6 - This will most likly fail becasue of PHPUnit version, so might need some help on that part to get this to pass 
 * [Tests] Change order of accept headers with q= arguments to get them to pass  This is caused by a minor sorting issue within Zeta Components on PHP 7: https://github.com/zetacomponents/MvcTools/issues/11 
 * [Tests] Tear down trigger entries after test 
 * EZP-27975: Support post_max_size value set to 0 during content/publish in UI 
 * Merge pull request #1321 from ezsystems/travis_php7  [Travis] Setup PHP7 testing on travis, bump requriment to PHP 5.5 
 * Fix EZP-27803: Wrong URLs in Treemenu if Platform is installed in a sub-directory (#1318) 
 * EZP-27975: Correct logic of c95ee808ea836a2f3d3b83876bafbfb208dde4a0 (#1323) 
 * EZP-24744: Increase password security  (cherry picked from commit 7d5bd88cb7ceba3e3ec1c349786e1022d375d38f) 
 * eZUser::hashType() lacks PASSWORD_HASH_MYSQL case  Use passwordHashTypeID() instead which has the case, thereby also reducing redundancy. 
 * Remove duplicated mysql login clause 
 * Merge pull request #1322 from ezsystems/ezp24744-increase_password_security  EZP-24744 - Increase password security 
 * Bump master to 2017.10 (#1325) 
 * Update password hash type to current default when changing the user password (#1326) 
 * Support MySQL error codes in DB exceptions (#1315) 
 * Fix - Typo in  ezobjectrelation.tpl (#1328)  on closing bracket too much 
 * Bump version to 2017.10 
 * Remove wrong semicolons in standard design (#1327) 
 * Making the constructor parameter $row optional (#1330) 
 * Fix EZP-28165: Incorrect increment of object_count in ezsearch_word (#1319)  * Fix for incorrect increment of object_count in ezsearch_word  * Do not rename variable 
 * Fix EZP-27993: Support quality override by image alias (#1324) 
 * [EZP-28180] Adding function "create" to simplify the creation of a ezsitedata entry (#1273)  * Adding function "createNew" to simplify the creation of a ezsitedata entry  * Revert to ugly array definition ;)  * Code makeup - tab to spaces 
 * EZP-28174: Improved error message when target of eznode:// and ezobject:// links doesn't exists (#1333) 
 * EZP-28470: Allow state limitation on manage_locations (#1335)  https://jira.ez.no/browse/EZP-28470 
 * Remove usage of each() function to avoid deprecations on PHP 7.2 (#1336) 
 * Bump master to 2017.12 (#1337) 
 * EZP-28214: Password hash silently defaults to MD5 (#1334) 
 * datetime.ini: Inline doc added. Better examples (#1331)  * Inline doc added. Better examples  * Fixing the Norwegian-English spelling mistakes.  * Re-add 'all' configuration  So thingin' breaks 
 * Fix EZP-26209: Textline legacy search indexing causes TransactionError (#1338) 
 * CSS Compressing was ignoring @2x image path (#1244)  image path like background-image: url('../images/sample@2x.png'); wasnt replaced during CSS Compressing 
 * Add compat layer for constructor for eZSiteInstaller (#1339)  * Add compat layer for constructor for eZSiteInstaller  Given this is used by packaged we need to make sure it still works with old style.  * Update ezsiteinstaller.php  * Update ezsiteinstaller.php 
 * Fix warning on counting NULL in PHP 7.2 (#1341) 
 * [Feature] Change to use Composer for autoload, drop PEAR/EZC (#1340)  * [Feature] Add support for autloading via Composer  * Cleanup all pear and bundeld ezc code, not relevant anymore  * Update autoload.php  * Update config.php-RECOMMENDED 
 * Fix EZP-28750: Draft preview displays wrong Version information (#1344) 
 * EZP-28736: SPI cache not cleared when a content is published via a delayed publication workflow (#1345) 
 * Updated license badge in README (#1346) 
 * [Composer] Update to GPL-2.0-only license notation for packagist 
 * Typo in variable name results in PHP Notice and unneeded file deletion checks on ezbinaryfile store/publish. (#1347) 
 * Add doc/feature/5.90/event.md 
 * Update doc on releases, incl explain internal 5.90 release version 
 * [Doc] Add missing bc doc on PHP7 changes in 2017.10 
 * EZP-20442: Matrix labels are not washed 
 * EZP-27877: Forgotpassword hash is insecure 
 * EZP-28108: Submitting form with ContentObjectID changed sends e-mail anyway 
 * EZP-28298: Legacy leaks object names by object link/embed  Fix object name/path leakage for object and node links and embeds Fix object name leakage for embeds in eZ OE Fix object name leakage for links/embeds in related objects list 
 * EZP-28950: Add support for utf8mb4 charset to i18n (#1361) 
 * EZP-28950: Settings example for blocking 4-byte chars in login 
 * EZP-29039: Image aliases expiry on draft discard (#1358)  * EZP-29039: Image aliases expiry on draft discard  * Use correct flag for htmlspecialchars 
 * [Composer] Bump conflict to kernel <7.2 & version to 2018.06 
 * Bump version to 2018.06 
 * Relax conflict with kernel  Until there are is a strong dependency on the utf8mb4 changes _(or other changes to shared features)_. 
 * EZP-29210: Fix admin UI anchor link scroll bug (#1366)  * Fix admin UI anchor link scroll bug - fixed height on the width controller element causes the user to get 'stuck' and unable to scroll back to the top when clicking on a link targeting an in page anchor  The bug is also noted in this Jira ticket: https://jira.ez.no/browse/EZP-29210  * Fix admin UI anchor link scroll bug - removed widthcontrol-handler from user menu include template (admin) 
 * Fix regression which broke PASSWORD_HASH_MD5_PASSWORD (#1354) 
 * EZP-29033: [Legacy] Don't remove links user has no access to (#1355)  * EZP-29033: [Legacy] Don't remove links user has no access to  * fixup! EZP-29033: [Legacy] Don't remove links user has no access to 
 * Fix link source change (object => node) when editing a version (#1291) 
 * Update search index in hide cronjob (#1348)  * Fix link source change (object => node) when editing a version  * Update search index in hide cronjob 
 * Force to use integer keys in eZXMLOutputHandler->NodeArray (#1356) 
 * Update index when adding/removing node assignment (#1353)  In the back-end, when you add a new location to an existing content, or remove one, the search index is not updated. 
 * Fixed fatal error (#1367)  `continue` used outside of loop. 
 * CS 
 * Fixed php7 warning (#1370)  * Fixed php7 warning  ErrorException (E_WARNING) Illegal string offset 'path'  * Moved declaration of $Result to top of case 
 * EZP-27101: Remove recursion when retrieving search metadata for relation fields (#1372)  * Fix EZP-27101: Remove recursion when retrieving search metadata  * fixup! Fix EZP-27101: Remove recursion when retrieving search metadata  * fixup! Fix EZP-27101: Remove recursion when retrieving search metadata 
 * Make query compatible with strict MySQL (#1368)  MySQL 5.7 is strict by default and will error out on the incorrect GROUP BY. 
 * [Composer] Bump conflict to kernel <6.13 & version to 2017.12 
 * EZP-29210: Fix admin UI anchor link scroll bug (#1366)  * Fix admin UI anchor link scroll bug - fixed height on the width controller element causes the user to get 'stuck' and unable to scroll back to the top when clicking on a link targeting an in page anchor  The bug is also noted in this Jira ticket: https://jira.ez.no/browse/EZP-29210  * Fix admin UI anchor link scroll bug - removed widthcontrol-handler from user menu include template (admin)  (cherry picked from commit a844df6a524698960a90d8430f403e46de1cd6f9) 
 * Fix regression which broke PASSWORD_HASH_MD5_PASSWORD (#1354)  (cherry picked from commit 0ffed7d752f80f635f99ec3c690713a0f21f9526) 
 * EZP-29033: [Legacy] Don't remove links user has no access to (#1355)  * EZP-29033: [Legacy] Don't remove links user has no access to  * fixup! EZP-29033: [Legacy] Don't remove links user has no access to  (cherry picked from commit 7814827a396b7333d380cb324b5cb226af42ec93) 
 * Fix link source change (object => node) when editing a version (#1291)  (cherry picked from commit 1e5fa83b8027e0536f897a580ff5e480e92af9c9) 
 * Update search index in hide cronjob (#1348)  * Update search index in hide cronjob  (cherry picked from commit 6f4e99fcd12e0ef70d41b7a74eb92d226a5177ab) 
 * Force to use integer keys in eZXMLOutputHandler->NodeArray (#1356)  (cherry picked from commit 4fcd80c575963097809df70539c99c46df4072b5) 
 * Update index when adding/removing node assignment (#1353)  In the back-end, when you add a new location to an existing content, or remove one, the search index is not updated.  (cherry picked from commit d1ed2aaada4b3e36c2a91539832394407445a7cf) 
 * Fixed fatal error (#1367)  `continue` used outside of loop.  (cherry picked from commit 582e393052c190812064a5fc91bcb848c80d789a) 
 * Fixed php7 warning (#1370)  * Fixed php7 warning  ErrorException (E_WARNING) Illegal string offset 'path'  * Moved declaration of $Result to top of case  (cherry picked from commit 69c2f36125f441a263194ac3ac9ab4e6d835ca75) 
 * Make query compatible with strict MySQL (#1368)  MySQL 5.7 is strict by default and will error out on the incorrect GROUP BY.  (cherry picked from commit 28fadf953a37ae4b3e138d1b9141b0c7b78999fc) 
 * EZP-29285: Custom tags: CTRL+Enter to get between 2 consecutive tags (#1369)  * EZP-29285: Custom tags: CTRL+Enter to get between 2 consecutive tags  * Update to make breakoutspace enabled by default  * add new line on last line in js file 
 * EZP-28706: Fixed hardcoded PostgreSQL sequence names (#1342)  * [PostgreSQL] Aligned sequence naming with ezpublish-kernel 7.x  * [PostgreSQL] Added upgrade script aligning PostgreSQL seq names with 7.x  * [PostgreSQL] Aligned Legacy Schema sequence naming with 7.x  * [PostgreSQL] Aligned ezenum table sequence name with 7.x 
 * Update conflict to depend on 7.2 for dependent changes  _(2017.12.x stays compatible with 6.13)_ 
 * Merge branch '2017.12' 
 * EZP-29170: Custom tag attribute of "link" type should indicate what node is selected (#1365) 
 * Skip tests that depend on critmon1.ez.no as it has been shut down. (#1374) 
 * EZP-29390: [Legacy] PHP Warning: Cannot change session name when session is active (#1373)  * [Legacy] PHP Warning: Cannot change session name when session is active  * Improved docblock  * fixup! Improved docblock  * fixup! [Legacy] PHP Warning: Cannot change session name when session is active 
 * EZP-29390: [Legacy] PHP Warning: Cannot change session name when session is active (#1373)  * [Legacy] PHP Warning: Cannot change session name when session is active  * Improved docblock  * fixup! Improved docblock  * fixup! [Legacy] PHP Warning: Cannot change session name when session is active  (cherry picked from commit 7bf5ae6d2afc37798cfcd7f4bf661f4fb038d965) 
 * Skip tests that depend on critmon1.ez.no as it has been shut down. (#1374)  (cherry picked from commit ea83877753e80722257b82c81aac48dbc72db84e) 
 * EZP-29400 : ezobjectrelationlist - fix fatal error (#1376) 
 * Merge branch '2017.12' 
 * Fix EZP-29418: Wrong Session Name after EZP-29390 (#1377) 
 * Merge branch '2017.12' 
 * Fix CS from #1377 
 * Merge branch '2017.12' 
 * EZP-29379: Fixed changing of section not committing to the solr index. (#1378) 
 * Merge branch '2017.12' 
 * [PHP] Fix some PHP 7.2 deprecations (#1382)  * [PHP] Change use of create_function for closure  * [PHP] Fix some usage of count  * CS  * Add symfony/polyfill-php73 in order to use is_countable() function  * Use ternary operator to shorten closure like others  * CS 
 * Merge branch '2017.12' 
 * EZP-29379: Option to skip search engine indexing inside eZContentOperationCollection::updateSection (#1381)  Followup for #1378 (0afc0aead1bc58d3c3e4a8b811882447ff1fd258) 
 * Merge branch '2017.12' 
 * Add a field to support "date object was trashed" (#1351) 
 * [Composer] Bump to 2018.09 with #1351 merged  Schema change, so bumping requirements on kernel to 7.3 
 * Prevent race condition when generating TS cache (#1388) 
 * Add BC constructor for eZContentUploadHandler (#1392)  * Add BC constructor for eZContentUploadHandler  To avoid issues for ezodf, add bc constructor.  See: https://github.com/ezsystems/ezodf/pull/18  * CS 
 * Merge branch '2017.12' 
 * Atomic cache writes to prevent corrupt caches. (#1389) 
 * [Travis] Enable test runs on 2017.12 
 * Merge branch '2017.12' 
 * Fix missing SQL updates / schema for trashed column (#1393) 
 * Fix update sql file name 
 * Update version number, add hint of which version it is 
 * Merge branch '2017.12' into 2018.09 
 * Merge branch '2018.09' 
 * Fix warnings about continue inside switch on PHP 7.3 (#1387) 
 * [PHP7] Add constructor compatibility on eZContentObjectTreeNode (#1395)  * [PHP7] Add ctor compat on eZContentObjectTreeNode * Fix recursion in tests by using parent:: 
 * Merge branch '2017.12' into 2018.09 
 * Merge branch '2018.09' 
 * EZP-29704: Passwordless login for LDAP users 
 * Merge branch '2017.12' into 2018.09 
 * Merge branch '2018.09' 
 * EZP-29699: XSS js vulnerability in 'module disabled' error template (#134) 
 * Merge branch '2017.12' into 2018.09 
 * Merge branch '2018.09' 
 * EZP-29552: Do not allow PHAR stream decoding 
 * Merge branch '2017.12' into 2018.09 
 * Merge branch '2018.09' 
 *  [ezpublish legacy] include assigned nodes into the removethis process (#1406)  * requirement ** node id is used as parameter ** used node id is the main one * context : known context is the unpublish feature 
 * Merge branch '2017.12' into 2018.09 
 * Merge branch '2018.09' 
 * Update header in regards to naming 
 * EZP-29957: Two XML blocks in the same Class in a different Categories will break second Online Editor (#1411) 
 * Merge branch '2017.12' into 2018.09 
 * Merge branch '2018.09' 
 * Revert "Prevent race condition when generating TS cache (#1388)"  This reverts commit 91205c0329fd08786d2d7cb01947a86fd059d20c.  Due to regression described in: - https://jira.ez.no/browse/EZP-29901 - https://github.com/ezsystems/ezpublish-legacy/pull/1409 - https://github.com/ezsystems/ezpublish-legacy/pull/1388#issuecomment-444615599 
 * Merge branch '2018.09' 
 * EZP-29573: Legacy installer should allow utf8mb4 charset (#1386) 
 * EZP-29573: Legacy installer should allow utf8mb4 charset (#1386) 
 * Merge branch '2017.12' into 2018.09 
 * Merge branch '2018.09' 
 * EZP-29723: Content object attributes not set properly (#1416) 
 * Merge branch '2017.12' into 2018.09 
 * Merge branch '2018.09' 
 * EZP-28681: Legacy transaction handling (#1415)  * EZP-29797: Legacy transaction handling: Discard draft (attribute delete) * EZP-29798: Legacy transaction handling: Discard draft * EZP-29799: Legacy transaction handling: Publish draft * EZP-29800: Legacy transaction handling: DFS cache purge * EZP-29796: Legacy transaction handling: Autosave 
 * Merge branch '2017.12' into 2018.09 
 * Merge branch '2018.09' 
 * Fix: Call count function on non countable variable  Fixes #1410 
 * Merge branch '2017.12' into 2018.09 
 * Merge branch '2018.09' 
 * EZP-29279: Values of Date Field Type should be handled in UTC only (#1401)  * EZP-29279: ezdate timestamps are now stored in UTC  * Added missing hours, minutes and seconds to Timestamp conversion and add more s p a c e s to the same class code  * Fixup after CR 
 * EZP-29096: Do not store empty draft values in DB (#1417)  * EZP-29096: Do not store empty draft values in DB 
 * EZP-30160 eZOE should work on Android (#1419) 
 * EZP-29096: Do not store empty draft values in DB (#1417)  * EZP-29096: Do not store empty draft values in DB  (cherry picked from commit 1adb33df7d48f8173f87bd841189bf602f197224) 
 * Merge branch '2017.12' into 2018.09 
 * Merge branch '2018.09' 
 * Forward ezsession::remove to session handlers (#1332)  * Forward ezsession::remove to session handlers  * Indenting fixed 
 * Avoid reflected XSS in DB handler error messages (#1425) 
 * Merge branch '2017.12' into 2018.09 
 * Merge branch '2018.09' 
 * [Composer] set master as 2019.03 
 * EZP-30316: Increase default minimum password length (legacy) (#1423) 
 * Fix EZP-30383: Change password with "legacy_mode: false" does not properly clear the cache (#1426) 
 * fix EZP-30293 : PHP Notice - Array to string (#1421)  legacy template navigator/google.tpl - PHP Notice - Array to string conversion ignoring new view_parameters value _custom 
 * Set version to 2019.03 
 * Avoid error in eZGeneralDigestHandler::storeSettings 
 *  Avoid warning in eZModuleParamsOperator 
 * Avoid warning in ezpContentFieldSet 
 * Refresh eZExpiryHandler instance after change siteaccess. 
 * Add ladino lad-IT locale settings in share 
 * Find cluster config file from $_SERVER param 
 * Create robots.txt 
 * Set max log file size Set EZP_INI_FILEMTIME_CHECK Set ImageMagick thread limit  Fix typo EZPUBLISH_LOG_ROTATE_FILES in ezdebug 
 * Ignore null filename or filepath Clusterize ezflowmedia 
 * Avoid useless reparenting in ezurlalias 
 * Add ezpEvent in eZScript 
 * remove default siteaccess folders: base mysite plain 
 * Merge pull request #1 from Opencontent/patch/avoid_general_digest_error  Avoid error in eZGeneralDigestHandler::storeSettings 
 * Merge pull request #2 from Opencontent/patch/avoid_module_params_warning   Avoid warning in eZModuleParamsOperator 
 * Merge pull request #3 from Opencontent/patch/avoid_ezpcontent_warning  Avoid warning in ezpContentFieldSet 
 * Merge pull request #4 from Opencontent/patch/refresh_expiry_per_siteaccess  Refresh eZExpiryHandler instance after change siteaccess. 
 * Merge pull request #5 from Opencontent/patch/add_ladino_locale  Add ladino lad-IT locale settings in share 
 * Merge pull request #6 from Opencontent/patch/customize_index_cluster  Find cluster config file from $_SERVER param 
 * Merge pull request #7 from Opencontent/patch/add_robots_txt  Create robots.txt 
 * Merge pull request #8 from Opencontent/patch/customize_config  Set max log file size 
 * Merge pull request #9 from Opencontent/patch/customize_clusterize  Ignore null filename or filepath 
 * Merge pull request #10 from Opencontent/patch/avoid_url_reparenting  Avoid useless reparenting in ezurlalias 
 * Merge pull request #11 from Opencontent/patch/add_ezpevent_in_ezscript  Add ezpEvent in eZScript 
 * Merge pull request #12 from Opencontent/patch/remove_default_siteaccess  remove default siteaccess folders: base mysite plain 
 * Merge with ez 
 * fix cluster_config file finder and php deprecation 
 * Merge pull request #13 from Opencontent/patch/customize_index_cluster  fix cluster_config file finder and php deprecation 
 * Customize gitignore 
 * Merge pull request #14 from Opencontent/patch/customize_gitignore  Customize gitignore 

### [gggeek/ggwebservices changes between 0.14.2 and 01a5f39](https://github.com/Opencontent/ggwebservices/compare/0.14.2...01a5f39)
 * update license tag 
 * Fix php7 constructor 

### [netgen/ngpush changes between 1.3 and 99c87d4](https://github.com/Opencontent/ngpush/compare/1.3...99c87d4)
 * changed outdated permissions 
 * Changed the preview image to the default alias small. 
 * revert alias change as the alias is defined in the extension 
 * Merge pull request #5 from styleflashernewmedia/master  Updated outdated facebook permissions. 
 * fix php7 constructor 

### [opencontent/bfsurveyfile changes between 1.0 and 2.1](https://github.com/Opencontent/bfsurveyfile/compare/1.0...2.1)
 * fix PHP 4 constructors 
 * Fix int cast + avoid die() 

### [opencontent/cjwnewsletter-ls changes between 2.3.3 and 3.0.1](https://github.com/Opencontent/cjw_newsletter/compare/2.3.3...3.0.1)
 * fix PHP 4 constructors 
 * Merge branch 'fix' 

### [opencontent/easyvocs_connector-ls changes between 65f5e25 and 97e5043](https://github.com/OpencontentCoop/easyvocs_connector/compare/65f5e25...97e5043)
 * Add license 

### [opencontent/enhancedezbinaryfile-ls changes between 1.0 and 2.0](https://github.com/Opencontent/enhancedezbinaryfile/compare/1.0...2.0)
 * fix PHP 4 constructors 

### [opencontent/ezflowplayer-ls changes between 1.0.1 and 2.0](https://github.com/Opencontent/ezflowplayer/compare/1.0.1...2.0)
 * fix PHP 4 constructors 

### [opencontent/occhangeobjectdate-ls changes between 1.1 and 2.0](https://github.com/OpencontentCoop/occhangeobjectdate/compare/1.1...2.0)
 * Aggiunto replace_in_attribute che permette di sostituire un stringa dentro un attributo ezstring, con una stringa data 
 * correzine commit per file assente 
 * fix PHP 4 constructors 

### [opencontent/occsvimport-ls changes between 1.2.2 and 2.1](https://github.com/OpencontentCoop/occsvimport/compare/1.2.2...2.1)
 * fix PHP 4 constructors 
 * Update empty ezurl 
 * Merge pull request #2 from OpencontentCoop/hotfix/ezurl  Update empty ezurl 
 * First check relation by remoteId 
 * Merge pull request #4 from OpencontentCoop/relations_by_remote  First check relation by remoteId 
 * import from google spreadsheets (#3) 

### [opencontent/oceditorialstuff-ls changes between 1.2.2 and 2.1.2](https://github.com/OpencontentCoop/oceditorialstuff/compare/1.2.2...2.1.2)
 * fix PHP 4 constructors 
 * Translations 
 * tabdrop only on nav-tab (designitalia conflict) 
 * fix constructors 

### [opencontent/ocgdprtools-ls changes between b50d386 and 1.2.1](https://github.com/OpencontentCoop/ocgdprtools/compare/b50d386...1.2.1)
 * Add license 
 * fix confirm publish template in admin design 

### [opencontent/ocmultibinary-ls changes between 1.2.1 and 2.0.1](https://github.com/OpencontentCoop/ocmultibinary/compare/1.2.1...2.0.1)
 * fix PHP 4 constructors 
 * hotfix fromString separator 

### [opencontent/ocoperatorscollection-ls changes between 1.3 and 2.1](https://github.com/OpencontentCoop/ocoperatorscollection/compare/1.3...2.1)
 * fix PHP 4 constructors 
 * add switch/user in ezaudit 
 * Merge pull request #5 from OpencontentCoop/audit_switch_user  add switch/user in ezaudit 

### [opencontent/ocsupport-ls changes between 3d1b588 and 49a42aa](https://github.com/OpencontentCoop/ocsupport/compare/3d1b588...49a42aa)
 * Add license 

### [opencontent/openpa_designitalia-ls changes between 1.23.6.1 and 1.23.8](https://github.com/OpencontentCoop/openpa_designitalia/compare/1.23.6.1...1.23.8)
 * gdpr confirm publish tpl 
 * Fix dependency constraint in composer.json 
 * minor bug fixes in survey templates and translations 

### [opencontent/openpa_newsletter-ls changes between 2.6.3 and 2.6.4](https://github.com/OpencontentCoop/openpa_newsletter/compare/2.6.3...2.6.4)
 * typo fix in template header 

### [opencontent/openpa_sensor-ls changes between 1.5 and 1.5.1](https://github.com/OpencontentCoop/openpa_sensor/compare/1.5...1.5.1)
 * fix node/view/full template path 

### [opencontent/openpa_theme_2014-ls changes between 2.11.9 and 2.11.10](https://github.com/OpencontentCoop/openpa_theme_2014/compare/2.11.9...2.11.10)
 * Fix dependency constraint in composer.json 

